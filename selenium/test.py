import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.options import Options
from time import sleep
import requests

options = Options()
options.headless = True
driver = webdriver.Firefox(options=options)


class Selenium(unittest.TestCase):

    def test1(self):
        driver.get('https://www.aroundatx.live')
        result = driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    def test2(self):
        driver.get('https://www.aroundatx.live')
        result = driver.find_element_by_class_name('App')
        self.assertNotEqual(result, None)

    def test3(self):
        driver.get('https://www.aroundatx.live')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'card')))
        result = driver.find_elements_by_class_name('card')
        self.assertEqual(len(result), 3)
        
    def test4(self):
        driver.get('https://www.aroundatx.live')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'nav-link')))
        result = driver.find_elements_by_class_name('nav-link')
        self.assertEqual(len(result), 7)
        
    def test5(self):
        driver.get('https://www.aroundatx.live/about')
        WebDriverWait(driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'member-card')))
        result = driver.find_elements_by_class_name('member-card')
        self.assertEqual(len(result), 5)
        
    def test6(self):
        driver.get('https://www.aroundatx.live/hotels')
        result = driver.find_element_by_class_name('page-title').get_attribute('innerHTML')
        self.assertEqual(result, 'Hotels')

    def test7(self):
        driver.get('https://www.aroundatx.live/restaurants')
        result = driver.find_element_by_tag_name('h2').get_attribute('innerHTML')
        self.assertEqual(result, 'Restaurants')

    # def test8(self):
    #     driver.get('https://www.aroundatx.live/incidents')
    #     result = driver.find_element_by_tag_name('h1').get_attribute('innerHTML')
    #     self.assertEqual(result, 'Incidents')

if __name__ == '__main__':
    unittest.main()
