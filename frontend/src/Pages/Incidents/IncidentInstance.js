import React, {Component} from 'react';
import Leaflet from '../../Components/Leaflet'
import axios from 'axios';
import dish from "../Assets/dish.png"
import bed from "../Assets/bed.png"
import Card from "react-bootstrap/Card"
import 'leaflet/dist/leaflet.css';

class IncidentInstance extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            hotelData: null,
            restaurantData: null,
            incidentData: null
        }
    }

    async componentDidMount() {
        const url = window.location.href
        const idStartIndex = url.lastIndexOf('/') + 1
        const id = url.substring(idStartIndex)
        const incidentRes = await axios.get(`/api/incidents/id=${id}`);
        const hotelRes = await axios.get(`/api/hotels/zipcode=${incidentRes.data['zipcode']}`);
        const restaurantRes = await axios.get(`/api/restaurants/zipcode=${incidentRes.data['zipcode']}`);

        this.setState({
            incidentData: incidentRes.data,
            hotelData: hotelRes.data["hotels"],
            restaurantData: restaurantRes.data["restaurants"]
        })
    }

    displayHotel() {
        if (this.state.hotelData == null) {
            return null;
        } else if (this.state.hotelData.length === 0) {
            return (<p>No hotels found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.hotelData.length; i++) {
            let hotel = this.state.hotelData[i];
            item.push(<React.Fragment>
                    <li><a href={"/hotels/" + hotel["hotel_id"]}>{hotel["name"]}</a></li>
                    </React.Fragment>);
        }
        return item.slice(0, 5);
    }

    capitalizeFirstLetters(text) {
        return text.toLowerCase().replace(/(^[a-z]| [a-z])/g, function (match, letter) {    
            return letter.toUpperCase();
        });
    }

    displayRestaurant() {
        if (this.state.restaurantData == null) {
            return null;
        } else if (this.state.restaurantData.length === 0) {
            return (<p>No restaurants found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.restaurantData.length; i++) {
            let restaurant = this.state.restaurantData[i];
            item.push (
                <React.Fragment>
                    <li>
                        <a href={"/restaurants/" + restaurant["restaurant_id"]}>{restaurant["name"]}</a>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    render() {
        let hotelData = this.state.hotelData;
        let restaurantData = this.state.restaurantData;
        let incidentData = this.state.incidentData;

        return (hotelData != null && restaurantData != null && incidentData != null) ? (
            <React.Fragment>
                <div style={{margin: 'auto', marginTop: '10vh', width: '90%'}}>
                    <h1 className="text-center">{incidentData.name}</h1>
                    <h4 className="text-center">Scroll down to view more information!</h4>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
                    <Leaflet latitude={incidentData.latitude.toFixed(2)} longitude={incidentData.longitude.toFixed(2)}></Leaflet>
                    <div className="row">
                        <div className="col-sm">
                            <Card.Body>
                                <Card.Title ><h2>Location</h2></Card.Title>
                                <Card.Text style={{fontSize:"25px"}}>
                                    {this.capitalizeFirstLetters(incidentData.address)}<br></br>
                                    {this.capitalizeFirstLetters(incidentData.county) + " County"}<br></br>
                                    {"Latitude: " + incidentData.latitude}<br></br>
                                    {"Longitude: " + incidentData.longitude}<br></br><br></br><br></br><br></br>
                                    <div class="grid" style={{alignContent:'center'}}>
                                        <div class="row align-items-center">
                                            <div class="col-xl-6">
                                                <Card.Img 
                                                    variant="top" 
                                                    className="center" 
                                                    src={`https://maps.googleapis.com/maps/api/streetview?size=500x500
                                                        &location=${incidentData.latitude}, ${incidentData.longitude}
                                                        &key=AIzaSyBq7AIiCE5-MJFu-zme3caAJdD5Bivv2gw`} 
                                                    style={{width: "70%", height: "25%"}}
                                                /><br></br>
                                            </div>
                                            <div class="col-xl-6">
                                                <Card.Title><h2><br></br>Information</h2></Card.Title>
                                                {"ID: " + incidentData.code}<br></br>
                                                {"Department: " + incidentData.department}<br></br>
                                                {"Council District: " + incidentData.council_district}<br></br>
                                                {"Reported By: " + incidentData.method_recieved}<br></br>
                                                {"Created: " + incidentData.created}<br></br>
                                                {"Closed: " + incidentData.closed}<br></br><br></br><br></br><br></br><br></br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid" style={{alignContent:'center'}}>
                                        <div class="row align-items-start">
                                            <div class="col-xl-6">
                                                <Card.Img variant="top" className="center" src={bed} style={{width: "15%"}}/>
                                                <Card.Title><h2><br></br>Nearby Hotels</h2></Card.Title>
                                                <div style={{listStyle:"none"}}>{this.displayHotel()}</div><br></br><br></br><br></br>
                                            </div>
                                            <div class="col-xl-6">
                                                <Card.Img variant="top" className="center" src={dish} style={{width: "15%"}}/>
                                                <Card.Title><h2><br></br>Nearby Restaurants</h2></Card.Title>
                                                <div style={{listStyle:"none"}}>{this.displayRestaurant()}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </div> 
                    </div> 
                </div>
            </React.Fragment>
        ) : (
            <div>
                <h2><br></br><br></br><br></br>Loading...</h2>
            </div>
        );
    }
}

export default IncidentInstance;
