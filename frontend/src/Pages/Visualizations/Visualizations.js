import React from "react";
import RestaurantsChart from "./Around ATX/RestaurantsChart";
import HotelsChart from "./Around ATX/HotelsChart";
import IncidentsChart from "./Around ATX/IncidentsChart";

export default function Visualizations() {
    return (
        <React.Fragment>
            <div style={{margin: 'auto', marginTop: '10vh', width: '90%'}}>
                <h1 className="text-center">Our Visualizations</h1>
            </div>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    <RestaurantsChart></RestaurantsChart>
            </div>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    <HotelsChart></HotelsChart>
            </div>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    <IncidentsChart></IncidentsChart>
            </div>
        </React.Fragment>
    )
}

