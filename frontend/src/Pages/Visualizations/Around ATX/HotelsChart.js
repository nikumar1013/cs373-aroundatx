import React, {useEffect, useState} from "react";
import axios from "axios";
import {
    Scatter,
    ScatterChart,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip
} from "recharts";

export default function HotelsChart() {

    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('/api/hotels').then(response => {
                setData(response.data.hotels)
            }).catch((error) => {
                setData([])
            });
        }
        fetchData();
    }, []);

    function getValues() {
        var values = [];

        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var value = {"Guest Rating" : item['guest_rating'], "Distance to Airport" : item['distance_to_airport']};
            values.push(value);
        }

        return values;
    }

    return (
        <div style={{margin: 'auto', marginTop: '5vh'}} >
            <h3 className="text-center"> Hotel Guest Ratings Related to Distance To Airport </h3>
            <p className="text-center"> Hover to view exact values</p>
            <p className="text-center"> (x - Guest Rating, y - Distance To Airport) </p>
            <ScatterChart
                width={1000}
                height={500}
                data={getValues()}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <CartesianGrid />
				<XAxis type="number" dataKey="Guest Rating" />
				<YAxis type="number" dataKey="Distance to Airport" />
				<Tooltip cursor={{ strokeDasharray: '3 3' }} />
				<Scatter name="Plot" data={getValues()} fill="#bf5700" />
            </ScatterChart>
        </div>
    );
}
