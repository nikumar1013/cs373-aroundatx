import React, { Component } from "react";
import axios from "axios";
import {
    Scatter,
    ScatterChart,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip
} from "recharts";

class SongsChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const url = 'https://spotifynder.com/api/songs';
        const result = await axios.get(url);

        this.setState({
            data: result.data.message
        })
    }

    getValues() {
        var values = [];

        for (var i = 0; i < this.state.data.length; i++) {
            var item = this.state.data[i];
            var value = {"energy" : item['energy'], "danceability" : item['danceability']};
            values.push(value);
        }

        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div style={{ margin: 'auto', marginTop: '5vh' }} >
                <h3 className="text-center"> Song Energy Related to Danceability </h3>
                <p className="text-center"> Hover to view exact values</p>
                <p className="text-center"> (x - Song Energy, y - Song Danceability)</p>
                <ScatterChart
                    width={1000}
                    height={500}
                    data={this.getValues()}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                    <CartesianGrid />
                    <XAxis type="number" dataKey="energy" />
                    <YAxis type="number" dataKey="danceability" />
                    <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                    <Scatter name="Plot" data={this.getValues()} fill="#1DB954" />
                </ScatterChart>
            </div>
        ) : (
            <div>
            </div>
        );
    }
}

export default SongsChart;
