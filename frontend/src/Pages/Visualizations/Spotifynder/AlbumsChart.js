import React, { Component } from "react";
import axios from "axios";
import { Pie, PieChart, Tooltip } from "recharts";

class AlbumsChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const url = 'https://spotifynder.com/api/albums';
        const result = await axios.get(url);

        this.setState({
            data: result.data.message
        })
    }

    getValues() {
        var values = [];
        var dict = {};

        for (var i = 0; i < this.state.data.length; i++) {
            var genre = this.state.data[i]['genres'];
            if (genre in dict) {
                dict[genre]++;
            }
            else {
                dict[genre] = 0;
            }
        }

        for (var genre in dict) {
            var randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            values.push({"name": genre, "value": dict[genre], "fill": randomColor})
        }

        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div style={{ margin: 'auto', marginTop: '5vh' }} >
                <h3 className="text-center"> Frequency of Each Album Genre </h3>
                <p className="text-center"> Hover to view values</p>
                <PieChart
                    width={1000}
                    height={800}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                    <Pie style={{ paddingLeft: '50px' }}
                        dataKey="value"
                        isAnimationActive={true}
                        data={this.getValues()}
                        cx={485}
                        cy={350}
                        outerRadius={300}
                        fill="#1DB954"
                        label
                    />
                    <Tooltip />
                </PieChart>
            </div>
        ) : (
            <div>
            </div>
        );
    }
}

export default AlbumsChart;