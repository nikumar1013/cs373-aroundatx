import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import './Hotels.css';
import { Card, Col } from "react-bootstrap";

class HotelCard extends Component {
	constructor(props) {
		super(props);
		this.props = {
			data: null
		}
		this.state = {
			commits: null,
			issues: null,
			tests: null,
			hasAllData: false
		}
	}

	render() {
		return (
			<Col>
				<Card className="mb-2" style={{height:"300px", width:"300px"}}>
					<Card.Img variant="top" src={this.props.data["image"]} alt="profile-image" style={{height:"40%", width:"auto"}}/>
					<Card.Body>
						<Card.Title>
						<Link to={`/hotels/${this.props.data["hotel_id"]}`}><div>{this.props.data["name"]}</div></Link>
						</Card.Title>
						<Card.Text>
							Latitude: {this.props.data["latitude"]} <br />
							Longitude: {this.props.data["longitude"]} <br />
							Star Rating: {this.props.data["star_rating"]}/5 <br />
							Guest Rating: {this.props.data["guest_rating"]}/10 <br />
							Distance to City Center: {this.props.data["distance_to_city_center"]} miles <br />
							Distance to Airport: {this.props.data["distance_to_airport"]} miles <br />
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		)
	}
}

export default HotelCard;