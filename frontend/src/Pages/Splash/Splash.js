import React from "react"
import "./Splash.css";
import Typing from 'react-typing-animation';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
// from flaticon.com
import bed from "../Assets/bed.png"
import dish from "../Assets/dish.png"
import alert from "../Assets/alert.png"


function Splash() {
    return (
        <header className="home-header">
            <div className="home-container">
                <div className="background-image">
                    <div className="container, text-center" style={{height:"100vh"}}>
                        <div className="text-container">
                            <Typing speed={50}>
                                <span className="home-title">Around ATX</span>
                            </Typing>
                            <h3 className="tagline">Finding you the best hotels and restaurants around Austin.</h3>
                        </div>
                    </div>
                    <div className="container, text-center" style={{height:"70vh", width:"60vw", opacity:".90"}}>
                        <div className="row">
                            <div className="col-sm">
                                <Card className='mb-4' style={{minWidth: "15vw"}}>
                                <Card.Img variant="top" className="center" src={bed} style={{width: "8vw", margin:"auto", marginTop:"5%"}}/>
                                <Card.Body>
                                    <Card.Title>
                                    <Button variant="primary" href="/hotels">Hotels</Button>
                                    </Card.Title>
                                    <Card.Text style={{fontSize:"15px"}}>{"Explore hotel stays"}</Card.Text>
                                </Card.Body>
                                </Card>
                            </div>
                            <div className="col-sm">
                                <Card className='mb-4' style={{minWidth: "15vw"}}>
                                <Card.Img variant="top" className="center" src={dish} style={{width: "8vw", margin:"auto", marginTop:"5%"}}/>
                                <Card.Body>
                                    <Card.Title>
                                    <Button variant="primary" href="/restaurants">Restaurants</Button>
                                    </Card.Title>
                                    <Card.Text style={{fontSize:"15px"}}>{"Find nearby restaurants"}</Card.Text>
                                </Card.Body>
                                </Card>
                            </div>
                            <div className="col-sm">
                                <Card style={{minWidth: "15vw"}}>
                                <Card.Img variant="top" className="center" src={alert} style={{width: "8vw", margin:"auto", marginTop:"5%"}}/>
                                <Card.Body>
                                    <Card.Title>
                                    <Button variant="primary" href="/incidents">Incidents</Button>
                                    </Card.Title>
                                    <Card.Text style={{fontSize:"15px"}}>{"See nearby incidents"}</Card.Text>
                                </Card.Body>
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
      );
};
export default Splash;