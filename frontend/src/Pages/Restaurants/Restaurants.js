import React, {useEffect, useState} from 'react'
import axios from 'axios';
import MUIDataTable from "mui-datatables"; 
import Highlighter from 'react-highlight-words';
import dish from '../Assets/dish.png';
import 'bootstrap/dist/css/bootstrap.min.css';

function Restaurants() {
    const [data, setData] = useState([]);
    const [searchText, setSearchText] = useState("");

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('/api/restaurants').then(response => {
                setData(response.data.restaurants)
            }).catch((error) => {
                setData([])
            });
        }
        fetchData();
    }, []);

    const restaurantCustomBodyRender = (value, tableMeta, updateValue) => (
        <div>
            <Highlighter
                highlightClassName="highlight-class"
                searchWords={[searchText]}
                textToHighlight={value + ""}
            ></Highlighter>
        </div>
    );

    const columns = [
        {
            name: 'restaurant_id',
            label: 'Restaurant id',
            options: {
                filter: false,
                sort: false,
                display: "excluded",
            },
        },
        {
            name: 'name',
            label: 'Name',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: { 
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(name, filters) {
                        const show = 
                            (filters.indexOf('A-I') >= 0 && 
                                name.charCodeAt(0) >= "A".charCodeAt(0) && 
                                name.charCodeAt(0) <= "I".charCodeAt(0)) ||
                            (filters.indexOf('J-R') >= 0 && 
                                name.charCodeAt(0) >= "J".charCodeAt(0) && 
                                name.charCodeAt(0) <= "R".charCodeAt(0)) ||              
                            (filters.indexOf('S-Z') >= 0 && 
                                name.charCodeAt(0) >= "S".charCodeAt(0) && 
                                name.charCodeAt(0) <= "Z".charCodeAt(0));
                        return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    restaurantCustomBodyRender(value, tableMeta, updateValue),
            }
        },
        {
            name: 'rating',
            label: 'Rating (out of 5)',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ['3', '3.5', '4', '4.5', '5'],
                    logic(rating, filters) {
                      const show =
                        (filters.indexOf('3') >= 0 && rating === 3) ||
                        (filters.indexOf('3.5') >= 0 && rating === 3.5) ||
                        (filters.indexOf('4') >= 0 && rating === 4) ||
                        (filters.indexOf('4.5') >= 0 && rating === 4.5) ||
                        (filters.indexOf('5') >= 0 && rating === 5)
                      return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    restaurantCustomBodyRender(value, tableMeta, updateValue),
            }
        },
        {
            name: 'review_count',
            label: 'Review Count',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ["Low", "Medium", "High"],
                    logic(review_count, filters) {
                      const show =
                        (filters.indexOf("Low") >= 0 && review_count < 1700) ||
                        (filters.indexOf("Medium") >= 0 && review_count >= 1700 && review_count < 3400) ||
                        (filters.indexOf("High") >= 0 && review_count >= 3400);
                      return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    restaurantCustomBodyRender(value, tableMeta, updateValue),
            }
        },
        {
            name: 'price',
            label: 'Price Level',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ["Cheap", "Moderate", "Expensive"],
                    logic(price, filters) {
                      const show =
                        (filters.indexOf("Cheap") >= 0 && price === '$') ||
                        (filters.indexOf("Moderate") >= 0 && price === '$$') ||
                        (filters.indexOf("Expensive") >= 0 && price === '$$$');
                      return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    restaurantCustomBodyRender(value, tableMeta, updateValue),
            }
        },
        {
            name: 'delivery',
            label: 'Delivery',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ["Yes", "No"],
                    logic(delivery, filters) {
                      const show =
                        (filters.indexOf("Yes") >= 0 && delivery === 'Yes') ||
                        (filters.indexOf("No") >= 0 && delivery === 'No');
                      return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    restaurantCustomBodyRender(value, tableMeta, updateValue),
            }
        },
    ];

    const options = {
        filter: true,
        filterType: 'multiselect',
        onRowClick: (rowData) => {
          window.location.assign("/restaurants/" + rowData[0]);
        },
        customSearch: (query, currentRow) => {
            let found = false;
            if (query.length < 2) {
                query = "";
            }
            setSearchText(query);
            currentRow.forEach((col) => {
              if (col.toString().toLowerCase().includes(query.toLowerCase())) {
                found = true;
              }
            });
            return found;
        },
        onSearchClose: () => {
            setSearchText("");
        },
        download: false,
        print: false,
        selectableRowsHideCheckboxes: true,
        selectableRowsHeader: false,
        viewColumns: false
    };

    return (
        <div className="body">
            <div className="table-container">
                <div class="grid" style={{alignContent:'center'}}>
                    <div class="row align-items-center">
                        <div class="col-xl-4">
                            <img src={dish} style={{ width: "40%", height: "40%" }} alt="Unable to load"></img>
                        </div>
                        <div class="col-xl-4">
                            <h2 className="page-title">Restaurants</h2>
                            <p style={{textAlign:"center"}}>Find amazing places to eat in Austin.</p>
                            <h6 style={{textAlign:"center"}}>Click a column title to sort and click the icons in the top right to search and/or filter</h6>
                        </div>
                        <div class="col-xl-4">
                            <img src={dish} style={{ width: "40%", height: "40%" }} alt="Unable to load"></img>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{display:'table', tableLayout:'fixed', width:'100%', height:'100%'}}>
                <MUIDataTable columns={columns} options={options} data={data}/>
            </div>
        </div>
    )
}

export default Restaurants;