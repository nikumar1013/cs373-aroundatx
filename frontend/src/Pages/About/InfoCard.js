import React, { Component } from 'react';

const COMMIT_URL = "https://gitlab.com/api/v4/projects/24679574/repository/commits?per_page=1000";
const ISSUE_URL = "https://gitlab.com/api/v4/projects/24679574/issues?per_page=1000&assignee_username=";
const GITLAB_URL = "https://gitlab.com/"
const MEMBER_INFO = require("./member-info.json");

class InfoCard extends Component {
    constructor(props) {
        super(props);

        this.props = {
            name: null,
            pic: null
        }

        this.state = {
            commits: null,
            issues: null,
            hasAllData: false
        }
    }

    async componentDidMount() {
        let commits = await this.fetchCommits();
        let issues = await this.fetchIssues();
        this.setState({ commits: commits, issues: issues, hasAllData: true })
    }

    async fetchCommits() {
        let response = await fetch(COMMIT_URL);
        let commits = await response.json();
        var total = 0;
        commits.forEach(commit => {
            if (MEMBER_INFO[this.props.name]["gitlab_name"][0] === "Tanmay Singh" && commit["author_email"] === "tstanmay13@gmail.com") {
                total++;
            }
            else if (MEMBER_INFO[this.props.name]["gitlab_name"].indexOf(commit["author_name"]) > -1) {
                total++;
            }
        });
        return total;
    }

    async fetchIssues() {
        let id = MEMBER_INFO[this.props.name]["gitlab_id"];
        let response = await fetch(ISSUE_URL + id);
        let json = await response.json();
        global.totalIssues += json.length;
        return json.length;
    }

    render() {
        return (
            <div className="col-lg-4">
                <div className="text-center card-box">
                    <div className="member-card pt-2 pb-2">
                        <div className="thumb-lg member-thumb mx-auto">
                            <img 
                                src={this.props.pic} 
                                className="rounded-circle img-thumbnail" 
                                alt="Unable to load" 
                            />
                        </div>
                        <div>
                            <h4>{this.props.name}</h4>
                            <p className="text-muted">{MEMBER_INFO[this.props.name]["role"]} </p>
                            <p className="text-muted">{MEMBER_INFO[this.props.name]["bio"]} </p>
                            <a 
                                href={GITLAB_URL + MEMBER_INFO[this.props.name]["gitlab_id"]} 
                                className="text-pink">{MEMBER_INFO[this.props.name]["gitlab_id"]} 
                            </a>
                            <p/>
                        </div>
                        <ul className="social-links list-inline">
                            <li className="list-inline-item">
                                <a 
                                    href={MEMBER_INFO[this.props.name]['linkedin']} 
                                    title data-placement="top" data-toggle="tooltip" 
                                    className="tooltips" data-original-title="LinkedIn">
                                    <i className="fa fa-linkedin" />
                                </a>
                            </li>
                        </ul>
                        <div className="mt-4">
                            <div className="row">
                                <div className="col-4">
                                    <div className="mt-3">
                                        <h4>{this.state.commits}</h4>
                                        <p className="mb-0 text-muted">Commits</p>
                                    </div>
                                </div>
                                <div className="col-4">
                                    <div className="mt-3">
                                        <h4>{this.state.issues}</h4>
                                        <p className="mb-0 text-muted">Issues</p>
                                    </div>
                                </div>
                                <div className="col-4">
                                    <div className="mt-3">
                                        <h4>{MEMBER_INFO[this.props.name]["tests"]}</h4>
                                        <p className="mb-0 text-muted">Unit Tests</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default InfoCard;

