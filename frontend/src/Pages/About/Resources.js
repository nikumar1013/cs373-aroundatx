import aws from './ToolsPics/aws.png'
import black from './ToolsPics/black.png'
import docker from './ToolsPics/docker.png'
import flask from './ToolsPics/flask.png'
import gitlab from './ToolsPics/GitLabLogo.png'
import google from './ToolsPics/google.png'
import incident from './ToolsPics/incident.png'
import marshmallow from './ToolsPics/marshmallow.png'
import namecom from './ToolsPics/namecom.jpg'
import postgre from './ToolsPics/postgre.png'
import postman from './ToolsPics/PostmanLogo.png'
import rapidapi from './ToolsPics/rapidapi.png'
import react from './ToolsPics/react.png'
import sqla from './ToolsPics/sqla.png'
import teams from './ToolsPics/teams.png'
import yelp from './ToolsPics/yelp.png'



const toolsInfo = [
	{
		title: "React",
		img: react,
		description: ": JavaScript library for front-end development",
		link: "https://reactjs.org/",
	},
	{
		title: "Flask",
		img: flask,
		description: ": Simple framework for API development",
		link: "https://flask.palletsprojects.com/en/1.1.x/",
	},
	{
		title: "SQLAlchemy",
		img: sqla,
		description: ": SQL toolkit and object-relational mapper",
		link: "https://www.sqlalchemy.org/",
	},
	{
		title: "Marshmallow",
		img: marshmallow,
		description: ": Library for complex datatype conversion",
		link: "https://marshmallow.readthedocs.io/en/stable/",
	},
	{
		title: "PostgreSQL",
		img: postgre,
		description: ": Relational database management system",
		link: "https://www.postgresql.org/",
	},
	{
		title: "Black",
		img: black,
		description: ": Python code formatter",
		link: "https://github.com/psf/black",
	},
	{
		title: "Postman",
		img: postman,
		description: ": Tool for designing and testing APIs",
		link: "https://postman.com/",
	},
	{
		title: "AWS",
		img: aws,
		description: ": Cloud hosting platform",
		link: "https://aws.amazon.com/",
	},
	{
		title: "Docker",
		img: docker,
		description:
			": Containerization tool for consistent runtime environments",
		link: "https://docker.com/",
	},
	{
		title: "GitLab",
		img: gitlab,
		description: ": Git repository and CI/CD platform",
		link: "https://gitlab.com/",
	},
	{
		title: "Teams",
		img: teams,
		description: ": Team communication platform",
		link: "https://www.microsoft.com/en-us/microsoft-teams/group-chat-software/",
	},
	{
		title: "Name.com",
		img: namecom,
		description: ": Domain name manager",
		link: "https://www.name.com/",
	},
]

const apiInfo = [
	{
		title: "Austin Incidents API",
		img: incident,
		description:
			": Used to find incidents around Austin",
		link: "https://data.austintexas.gov/City-Government/311-Unified-Data-Test-Site-2019/i26j-ai4z",
	},
	{
		title: "Google Maps API",
		img: google,
		description: ": Used to visualize hotel, restaurant, and incident locations",
		link: "https://developers.google.com/maps",
	},
	{
		title: "Yelp API",
		img: yelp,
		description:
			": Used to find restaurants around Austin",
		link: "https://www.yelp.com/fusion",
	},
	{
		title: "RapidAPI Hotels",
		img: rapidapi,
		description: ": Used to find hotels around Austin",
		link: "https://rapidapi.com/apidojo/api/hotels4/endpoints",
	},
]

export { toolsInfo, apiInfo }