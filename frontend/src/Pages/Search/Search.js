import React, { useState } from 'react';
import {Row, Container} from 'react-bootstrap';
import algoliasearch from 'algoliasearch/lite';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import SearchHotelCard from '../Hotels/SearchHotelCard.js';
import SearchRestaurantsCard from './SearchRestaurantsCard.js';
import SearchIncidentCard from './SearchIncidentCard.js';
import algolialogo from './algolialogo.png';
import Image from "react-bootstrap/Image";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../Hotels/Hotels.css';

const searchClient = algoliasearch('E5EGQ12D8H', '6e5cf15ccc243187e411d3049cfd95da');
const hotelIndex = searchClient.initIndex('hotel_Aroundatx');
const restaurantIndex = searchClient.initIndex('restaurants_Aroundatx');
const incidentsIndex = searchClient.initIndex('incident_Aroundatx');

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'block',
        margin: '0 auto',
        width: 400
    },
    input: {
        display: 'flex',
        margin: '0 auto',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function Search() {

    const classes = useStyles();
	const [hotelDisplayCard, setHotelDisplayCard] = useState(null);
	const [restaurantDisplayCard, setRestaurantDisplayCard] = useState(null);
	const [incidentDisplayCard, setIncidentDisplayCard] = useState(null);
    const [searchQuery, setSearchQuery] = useState("");


    // Slices the data, converts it to a specific form for rendering
	const sliceHotelData = (data) => {
		let postData = data.map(pd => 
			<React.Fragment>
				<SearchHotelCard hit={pd} query={searchQuery}/>
			</React.Fragment>
		);
		setHotelDisplayCard(postData);
	};

	const sliceRestaurantData = (data) => {
		let postData = data.map(pd => 
			<React.Fragment>
				<SearchRestaurantsCard hit={pd} query={searchQuery}/>
			</React.Fragment>
		);
		setRestaurantDisplayCard(postData);
	}

	const sliceIncidentData = (data) => {
		let postData = data.map(pd => 
			<React.Fragment>
				<SearchIncidentCard hit={pd} query={searchQuery}/>
			</React.Fragment>
		);
		setIncidentDisplayCard(postData);
	}

    // Handles the search
    function searchChangeHandler(event) {
        let query = event.target.value;
        if (query != null && query.length > 0) {
			setSearchQuery(query);

            hotelIndex.search(query).then(result => {
				sliceHotelData(result.hits)
            });
			
			restaurantIndex.search(query).then(result => {
				sliceRestaurantData(result.hits);
			});

			incidentsIndex.search(query).then(result => {
				sliceIncidentData(result.hits)
			})
		} else {
			setHotelDisplayCard(null);
			setRestaurantDisplayCard(null);
			setIncidentDisplayCard(null);
		}
    }

    return (
        <div className="body" style={{float:"center"}}>
            <div className="table-container">
                <h2 className="page-title">Search</h2>
                <Paper component="form" className={classes.root} >
                    <InputBase
                        className={classes.input}
                        placeholder="search here..."
                        inputProps={{ 'aria-label': 'search hotels' }}
						onChange={searchChangeHandler}
                    />
                </Paper>
			</div>
			<Container fluid>
				<h2>Hotels</h2>
                <Row className="hotel-row" md={5} lg={5}>
                    {hotelDisplayCard}
                </Row>
				<h2>Restaurants</h2>
				<Row className="hotel-row" md={5} lg={5}>
                    {restaurantDisplayCard}
                </Row>
				<h2>Incidents</h2>
				<Row className="hotel-row" md={5} lg={5}>
                    {incidentDisplayCard}
                </Row>
            </Container>
      		<br />
			<div className="algolialogo">
       		<div>Search Powered by &nbsp;</div>
    			<div>
          			<Image
            			src={algolialogo}
            			height="5%"
            			width="5%"
                    />
        		</div>
      		</div>
        </div>
    )
}
