# cs373-aroundatx

Repository for the CS373 Software Engineering IDB Project (Spring 2021, 11 AM, Group 17).

## Info

- Git SHA Phase 4: a442475854de970fbb16bcc2075f942a19b46b03
- Project leader: Nikhil Kumar
- GitLab pipelines: https://gitlab.com/jyotiluu/cs373-aroundatx/-/pipelines
- Website: https://aroundatx.live/
- API Documentation: https://documenter.getpostman.com/view/14748761/Tz5jfLdN
- Presentation: https://youtu.be/rsXZf3EBLlc

## Members

- Name: Nikhil Kumar
- EID: nk9373
- GitLab ID: @nikumar1013
- Estimated completion time: Phase 1 - 12 hours, Phase 2 - 20 hours, Phase 3 - 15 hours, Phase 4 - 10 hours
- Actual completion time: Phase 1 - 20 hours, Phase 2 - 35 hours, Phase 3 - 30 hours, Phase 4 - 20 hours

---

- Name: Jyoti Luu
- EID: jjl3377
- GitLab ID: @jyotiluu
- Estimated completion time: Phase 1 - 15 hours, Phase 2 - 25 hours, Phase 3 - 15 hours, Phase 4 - 10 hours
- Actual completion time: Phase 1 - 20 hours, Phase 2 - 25 hours, Phase 3 - 15 hours, Phase 4 - 10 hours

---

- Name: Tanmay Singh
- EID: tst36848
- GitLab ID: @tstanmay13 
- Estimated completion time: Phase 1 - 15 hours, Phase 2 - 25 hours, Phase 3 - 10 hours, Phase 4 - 10 hours
- Actual completion time: Phase 1 - 14 hours, Phase 2 - 20 hours, Phase 3 - 8 hours, Phase 4 - 8 hours

---

- Name: Pranay Kalagara
- EID: prk474
- GitLab ID: @pranaykalagara
- Estimated completion time: Phase 1 - 12 hours, Phase 2 - 25 hours, Phase 3 - 10 hours, Phase 4 - 10 hours
- Actual completion time: Phase 1 - 12 hours, Phase 2 - 22 hours, Phase 3 - 7 hours, Phase 4 - 7 hours

---

- Name: Jiaxi Chen
- EID: jc86629
- GitLab ID: @jiaxi312
- Estimated completion time: Phase 1 - 12 hours, Phase 2 - 20 hours, Phase 3 - 20 hours, Phase 4 - 12 hours
- Actual completion time: Phase 1 - 8 hours, Phase 2 - 27 hours, Phase 3 - 25 hours, Phase 4 - 10 hours
