#!/usr/bin/env python3

from unittest import main, TestCase
import requests
import json

class Tests(TestCase):

    ##### HOTELS ENDPOINT TESTS #####

    def test_num_hotels(self):
        response = requests.get("https://aroundatx.live/api/hotels")
        assert response.status_code == 200
        data = response.json()
        assert len(data["hotels"]) == 243

    def test_hotels_all(self):
        response = requests.get("https://aroundatx.live/api/hotels")
        assert response.status_code == 200
        data = response.json()
        assert data["hotels"][5] == {
            "hotel_id": 114313,
            "name": "Red Roof Inn Austin - Round Rock",
            "star_rating": 2.0,
            "street_address": "1990 North I-35",
            "city": "Round Rock",
            "zipcode": 78681,
            "state": "TX",
            "guest_rating": 6.0,
            "distance_to_city_center": 18.0,
            "distance_to_airport": 23.0,
            "neighborhood": "Round Rock",
            "latitude": 30.528,
            "longitude": -97.690978,
            "image": "https://thumbnails.trvl-media.com/A91Tv0SHmABWhRCJhKCMziRCTec=/250x140/smart/filters:quality(60)/images.trvl-media.com/hotels/1000000/50000/40500/40455/6d731321_z.jpg",
            "price": 56,
            "rooms_left": 1,
        }

    def test_hotels_id(self):
        response = requests.get("https://aroundatx.live/api/hotels/id=204749")
        assert response.status_code == 200
        data = response.json()
        assert data == {
            "hotel_id": 204749,
            "name": "Hilton Garden Inn Austin NW - Arboretum",
            "star_rating": 3.0,
            "street_address": "11617 Research Blvd",
            "city": "Austin",
            "zipcode": 78759,
            "state": "TX",
            "guest_rating": 8.8,
            "distance_to_city_center": 10.0,
            "distance_to_airport": 15.0,
            "neighborhood": "Mesa Park",
            "latitude": 30.416106,
            "longitude": -97.744278,
            "image": "https://thumbnails.trvl-media.com/3Jl0fsVrhKPlHESWY5HjFP9B6u8=/250x140/smart/filters:quality(60)/images.trvl-media.com/hotels/1000000/900000/896500/896494/2411368a_z.jpg",
            "price": 107,
            "rooms_left": 0,
        }

    def test_hotels_invalid_id(self):
        response = requests.get("https://aroundatx.live/api/hotels/id=00000")
        assert response.status_code == 404
        data = response.json()
        assert data == {"error": "00000 is not a valid hotel id"}

    def test_hotels_zipcode(self):
        response = requests.get("https://aroundatx.live/api/hotels/zipcode=78723")
        assert response.status_code == 200
        data = response.json()
        assert data["hotels"][0] == {
            "hotel_id": 118084,
            "name": "Rodeway Inn & Suites Downtown North",
            "star_rating": 2.0,
            "street_address": "6201 US 290 E",
            "city": "Austin",
            "zipcode": 78723,
            "state": "TX",
            "guest_rating": 5.2,
            "distance_to_city_center": 4.4,
            "distance_to_airport": 8.5,
            "neighborhood": "Windsor Park",
            "latitude": 30.32155,
            "longitude": -97.70256,
            "image": "https://thumbnails.trvl-media.com/_Mm0j7z0FU_TrgBvBogLHvuAC2E=/250x140/smart/filters:quality(60)/images.trvl-media.com/hotels/1000000/10000/5800/5702/86692a39_z.jpg",
            "price": 50,
            "rooms_left": 1,
        }

    def test_hotels_invalid_zipcode(self):
        response = requests.get("https://aroundatx.live/api/hotels/zipcode=ab")
        assert response.status_code == 404
        data = response.json()
        assert data == {"error": "invalid zipcode"}

    def test_hotels_search_sort(self):
        response = requests.get("http://aroundatx.live/api/hotels?sorty_by=midtown")
        assert response.status_code == 200
        data = response.json()
        assert data["hotels"][0] == {
            "hotel_id": 423707040,
            "name": "Village Motor Inn",
            "star_rating": 2.0,
            "street_address": "3012 S Congress Ave",
            "city": "Austin",
            "zipcode": 78704,
            "state": "TX",
            "guest_rating": 6.0,
            "distance_to_city_center": 2.7,
            "distance_to_airport": 5.9,
            "neighborhood": "Dawson",
            "latitude": 30.23228,
            "longitude": -97.75932,
            "image": "https://thumbnails.trvl-media.com/jX2zlir6qMRD3HIJFJhwNvhg_7A=/250x140/smart/filters:quality(60)/images.trvl-media.com/hotels/14000000/13210000/13209600/13209595/5ec9c08c_z.jpg",
            "price": 47,
            "rooms_left": 3,
        }

    def test_hotels_search_sort2(self):
        response = requests.get("http://aroundatx.live/api/hotels?star_rating=3")
        assert response.status_code == 200
        data = response.json()
        assert data["hotels"][1] == {
            "hotel_id": 112619,
            "name": "Motel 6 Austin, TX - Midtown",
            "star_rating": 2.5,
            "street_address": "7100 I 35 N",
            "city": "Austin",
            "zipcode": 78752,
            "state": "TX",
            "guest_rating": 6.4,
            "distance_to_city_center": 5.0,
            "distance_to_airport": 9.2,
            "neighborhood": "St. Johns",
            "latitude": 30.332352,
            "longitude": -97.705103,
            "image": "https://thumbnails.trvl-media.com/U66s52IwEoOj-NgUEwsQ7hm7J-0=/250x140/smart/filters:quality(60)/images.trvl-media.com/hotels/1000000/10000/500/414/91d20748_z.jpg",
            "price": 50,
            "rooms_left": 3,
        }

    ##### RESTAURANTS ENDPOINT TESTS #####

    def test_num_restaurants(self):
        response = requests.get("https://aroundatx.live/api/restaurants")
        assert response.status_code == 200
        data = response.json()
        assert len(data["restaurants"]) == 300

    def test_restaurants_all(self):
        response = requests.get("https://aroundatx.live/api/restaurants")
        assert response.status_code == 200
        data = response.json()
        assert data["restaurants"][6] == {
            "restaurant_id": "_LL0cTi1MTiIl-d3Px-QNA",
            "name": "Don Japanese Kitchen",
            "review_count": 211,
            "type": "Food Trucks",
            "rating": 4.5,
            "latitude": 30.286432,
            "longitude": -97.742651,
            "fulfillment": ["delivery"],
            "price": "$",
            "address": "411 W 23rd",
            "city": "Austin",
            "zipcode": 78705,
            "state": "TX",
            "phone": "",
            "image": "https://s3-media3.fl.yelpcdn.com/bphoto/yCsYKY_dWJdgs0lkACebHg/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/don-japanese-kitchen-austin?adjust_creative=jwVfZJG5lAN0EXteJ-YRaQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=jwVfZJG5lAN0EXteJ-YRaQ",
            "delivery": "Yes",
            "is_open": "Yes",
        }

    def test_restaurants_id(self):
        response = requests.get(
            "https://aroundatx.live/api/restaurants/id=v1UzkU8lEWdjxq8byWFOKg"
        )
        assert response.status_code == 200
        data = response.json()
        assert data == {
            "restaurant_id": "v1UzkU8lEWdjxq8byWFOKg",
            "name": "Gus's World Famous Fried Chicken",
            "review_count": 2502,
            "type": "Southern",
            "rating": 4.5,
            "latitude": 30.2634862,
            "longitude": -97.7417305,
            "fulfillment": ["delivery"],
            "price": "$",
            "address": "117 San Jacinto Blvd",
            "city": "Austin",
            "zipcode": 78701,
            "state": "TX",
            "phone": "+15124744877",
            "image": "https://s3-media1.fl.yelpcdn.com/bphoto/o-uJQMxHDGbGSHghhg1sgA/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/guss-world-famous-fried-chicken-austin?adjust_creative=jwVfZJG5lAN0EXteJ-YRaQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=jwVfZJG5lAN0EXteJ-YRaQ",
            "delivery": "Yes",
            "is_open": "Yes",
        }

    def test_restaurants_invalid_id(self):
        response = requests.get("https://aroundatx.live/api/restaurants/id=00000")
        assert response.status_code == 404
        data = response.json()
        assert data == {"error": "00000 not found"}

    def test_restaurants_zipcode(self):
        response = requests.get("https://aroundatx.live/api/restaurants/zipcode=78703")
        assert response.status_code == 200
        data = response.json()
        assert data["restaurants"][0] == {
            "restaurant_id": "ec6X_eh2wyzFLqHbApAsuw",
            "name": "Pepe\u2019s Tacos",
            "review_count": 69,
            "type": "Food Trucks",
            "rating": 4.5,
            "latitude": 30.273319,
            "longitude": -97.7535433,
            "fulfillment": ["pickup", "delivery"],
            "price": "-",
            "address": "704 N Lamar Blvd",
            "city": "Austin",
            "zipcode": 78703,
            "state": "TX",
            "phone": "+17372034175",
            "image": "https://s3-media3.fl.yelpcdn.com/bphoto/-0GKSi-XFVGGLtjdwwWiZA/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/pepe-s-tacos-austin?adjust_creative=jwVfZJG5lAN0EXteJ-YRaQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=jwVfZJG5lAN0EXteJ-YRaQ",
            "delivery": "Yes",
            "is_open": "Yes",
        }

    def test_restaurants_invalid_zipcode(self):
        response = requests.get("https://aroundatx.live/api/restaurants/zipcode=ab")
        assert response.status_code == 404
        data = response.json()
        assert data == {"error": "invalid zipcode"}

    def test_restaurants_search_sort(self):
        response = requests.get("http://aroundatx.live/api/restaurants?rating=4.5")
        assert response.status_code == 200
        data = response.json()
        assert data["restaurants"][1] == {
            "restaurant_id": "GbhTvJrSB7N67leOh2LZTg",
            "name": "Oh K Dog",
            "review_count": 24,
            "type": "Hot Dogs",
            "rating": 4.5,
            "latitude": 30.4774004,
            "longitude": -97.79737089999999,
            "fulfillment": [],
            "price": "-",
            "address": "11301 Lakeline Blvd",
            "city": "Austin",
            "zipcode": 78717,
            "state": "TX",
            "phone": "",
            "image": "https://s3-media1.fl.yelpcdn.com/bphoto/pe09g6ArPyioOOBKiZTQtw/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/oh-k-dog-austin-2?adjust_creative=jwVfZJG5lAN0EXteJ-YRaQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=jwVfZJG5lAN0EXteJ-YRaQ",
            "delivery": "No",
            "is_open": "Yes",
        }

    def test_restaurants_search_sort2(self):
        response = requests.get("http://aroundatx.live/api/restaurants?rating=4.0")
        assert response.status_code == 200
        data = response.json()
        assert data["restaurants"][4] == {
            "restaurant_id": "aByHmMVAUIVJrBjObf3S0Q",
            "name": "Chick'nCone - Austin",
            "review_count": 25,
            "type": "Chicken Shop",
            "rating": 4.0,
            "latitude": 30.28739,
            "longitude": -97.74184,
            "fulfillment": ["pickup", "delivery"],
            "price": "-",
            "address": "2348 Guadalupe St",
            "city": "Austin",
            "zipcode": 78705,
            "state": "TX",
            "phone": "+15129920055",
            "image": "https://s3-media2.fl.yelpcdn.com/bphoto/5g3dPm5jxQHsI1h3glLq6g/o.jpg",
            "yelp_url": "https://www.yelp.com/biz/chickncone-austin-austin?adjust_creative=jwVfZJG5lAN0EXteJ-YRaQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=jwVfZJG5lAN0EXteJ-YRaQ",
            "delivery": "Yes",
            "is_open": "Yes",
        }

    ##### INCIDENT ENDPOINT TESTS #####

    def test_num_incidents(self):
        response = requests.get("https://aroundatx.live/api/incidents")
        assert response.status_code == 200
        data = response.json()
        assert len(data["incidents"]) == 999

    def test_incidents_all(self):
        response = requests.get("https://aroundatx.live/api/incidents")
        assert response.status_code == 200
        data = response.json()
        assert data["incidents"][3] == {
            "incident_id": "21-00092630",
            "name": "ARR Missed Yard Trimmings/Compost",
            "code": "SWSYARDT",
            "department": "Austin Resource Recovery",
            "method_recieved": "Phone",
            "description": "New",
            "created": "Mon March 01, 2021 09:09 PM",
            "updated": "Mon March 01, 2021 09:09 PM",
            "closed": "Mon March 01, 2021 09:41 PM",
            "latitude": 30.17321328,
            "longitude": -97.91525893,
            "address": "6900 TRISSINO DR, AUSTIN, TX 78739",
            "city": "AUSTIN",
            "zipcode": "78739",
            "county": "TRAVIS",
            "council_district": 8,
        }

    def test_incidents_id(self):
        response = requests.get("https://aroundatx.live/api/incidents/id=21-00092634")
        assert response.status_code == 200
        data = response.json()
        assert data == {
            "incident_id": "21-00092634",
            "name": "Loose Dog",
            "code": "ACLONAG",
            "department": "Animal Services Office",
            "method_recieved": "Phone",
            "description": "Open",
            "created": "Mon March 01, 2021 09:15 PM",
            "updated": "Mon March 01, 2021 09:18 PM",
            "closed": "Mon March 01, 2021 09:41 PM",
            "latitude": 30.33232432,
            "longitude": -97.69964008,
            "address": "WILKS AVE & BENNETT AVE, AUSTIN, TX",
            "city": "AUSTIN",
            "zipcode": "78752",
            "county": "TRAVIS",
            "council_district": 4,
        }

    def test_incidents_invalid_id(self):
        response = requests.get("https://aroundatx.live/api/incidents/id=00000")
        assert response.status_code == 404
        data = response.json()
        assert data == {"error": "00000 not found"}

    def test_incidents_zipcode(self):
        response = requests.get("https://aroundatx.live/api/incidents/zipcode=78703")
        assert response.status_code == 200
        data = response.json()
        assert data["incidents"][0] == {
            "incident_id": "21-00092139",
            "name": "Concerns in the ROW",
            "code": "CONCERNS",
            "department": "Transportation",
            "method_recieved": "Phone",
            "description": "New",
            "created": "Mon March 01, 2021 04:47 PM",
            "updated": "Mon March 01, 2021 04:47 PM",
            "closed": "Mon March 01, 2021 09:41 PM",
            "latitude": 30.27965391,
            "longitude": -97.76550487,
            "address": "1808 W 8TH ST, AUSTIN, TX 78703",
            "city": "AUSTIN",
            "zipcode": "78703",
            "county": "TRAVIS",
            "council_district": 9,
        }

    def test_incidents_invalid_zipcode(self):
        response = requests.get("https://aroundatx.live/api/incidents/zipcode=ab")
        assert response.status_code == 404
        data = response.json()
        assert data == {"error": "invalid zipcode"}

    def test_incidents_search_sort(self):
        response = requests.get("http://aroundatx.live/api/incidents?county=travis")
        assert response.status_code == 200
        data = response.json()
        assert data["incidents"][0] == {
            "incident_id": "21-00092637",
            "name": "Animal Control - Assistance Request",
            "code": "ACINFORM",
            "department": "Animal Services Office",
            "method_recieved": "Phone",
            "description": "New",
            "created": "Mon March 01, 2021 09:21 PM",
            "updated": "Mon March 01, 2021 09:21 PM",
            "closed": "Mon March 01, 2021 09:41 PM",
            "latitude": 30.22692231,
            "longitude": -97.58765555,
            "address": "15007 JOLYNN ST, AUSTIN, TX 78725",
            "city": "AUSTIN",
            "zipcode": "78725",
            "county": "TRAVIS",
            "council_district": 0,
        }

    def test_incidents_search_sort(self):
        response = requests.get("http://aroundatx.live/api/incidents?county=travis")
        assert response.status_code == 200
        data = response.json()
        assert data["incidents"][1] == {
            "incident_id": "21-00092634",
            "name": "Loose Dog",
            "code": "ACLONAG",
            "department": "Animal Services Office",
            "method_recieved": "Phone",
            "description": "Open",
            "created": "Mon March 01, 2021 09:15 PM",
            "updated": "Mon March 01, 2021 09:18 PM",
            "closed": "Mon March 01, 2021 09:41 PM",
            "latitude": 30.33232432,
            "longitude": -97.69964008,
            "address": "WILKS AVE & BENNETT AVE, AUSTIN, TX",
            "city": "AUSTIN",
            "zipcode": "78752",
            "county": "TRAVIS",
            "council_district": 4,
        }


if __name__ == "__main__":  # pragma: no cover
    main()
