import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql+psycopg2://postgres:aroundatx@backend-db."
    "cvd9gpyejoue.us-east-2.rds.amazonaws.com:5432/postgres"
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Restaurants(db.Model):
    restaurant_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    review_count = db.Column(db.Integer)
    type = db.Column(db.String)
    rating = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    fulfillment = db.Column(db.JSON)
    price = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    state = db.Column(db.String)
    phone = db.Column(db.String)
    image = db.Column(db.String)
    yelp_url = db.Column(db.String)
    delivery = db.Column(db.String)
    is_open = db.Column(db.String)

    def __init__(
        self,
        restaurant_id,
        name,
        review_count,
        type,
        rating,
        latitude,
        longitude,
        fulfillment,
        price,
        address,
        city,
        zipcode,
        state,
        phone,
        image,
        yelp_url,
        delivery,
        is_open
    ):
        self.restaurant_id = restaurant_id
        self.name = name
        self.review_count = review_count
        self.type = type
        self.rating = rating
        self.latitude = latitude
        self.longitude = longitude
        self.fulfillment = fulfillment
        self.price = price
        self.address = address
        self.city = city
        self.zipcode = zipcode
        self.state = state
        self.phone = phone
        self.image = image
        self.yelp_url = yelp_url
        self.delivery = delivery
        self.is_open = is_open


db.create_all()
db.session.commit()


def insert_restaurants():
    with open("data/restaurants.json") as json_file:
        data = json.load(json_file)

    all_restaurants = []

    for item in data["businesses"]:
        restaurant_id = item["id"]
        name = item["name"]
        review_count = item["review_count"]
        type = item["categories"][0]["title"]
        rating = item["rating"]
        latitude = item["coordinates"]["latitude"]
        longitude = item["coordinates"]["longitude"]
        fulfillment = item["transactions"]

        if "price" not in item:
            price = "-"
        else:
            price = item["price"]

        address = item["location"]["address1"]
        zipcode = int(item["location"]["zip_code"])
        city = item["location"]["city"]
        state = item["location"]["state"]
        phone = item["phone"]
        image = item["image_url"]
        yelp_url = item["url"]

        if 'delivery' not in item['transactions']:
            delivery = "No"
        else:
            delivery = "Yes"
        
        if item["is_closed"]:
            is_open = "No"
        else:
            is_open = "Yes"


        restaurant = Restaurants(
            restaurant_id,
            name,
            review_count,
            type,
            rating,
            latitude,
            longitude,
            fulfillment,
            price,
            address,
            city,
            zipcode,
            state,
            phone,
            image,
            yelp_url,
            delivery, 
            is_open
        )

        all_restaurants.append(restaurant)

    db.session.add_all(all_restaurants)
    db.session.commit()


if __name__ == "__main__":
    insert_restaurants()
