FROM nikolaik/python-nodejs

RUN git clone https://gitlab.com/jyotiluu/cs373-aroundatx.git

WORKDIR /cs373-aroundatx

RUN git pull --force

ENV GENERATE_SOURCEMAP false

RUN cd frontend && npm install && npm install axios && npm install react-paginate && npm install algoliasearch && npm install react-instantsearch-dom && npm install @material-ui/icons && npm install @material-ui/core && npm install @material-ui/lab && npm install react-highlight-words && npm install recharts && npm run build

RUN pip3 install -r backend/requirements.txt

EXPOSE 80

CMD git pull --force && python3 backend/app.py
# COPY requirements.txt requirements.txt
# RUN pip install -r requirements.txt
# EXPOSE 8080
# COPY . .
# CMD ["gunicorn", "wsgi:app", "-w 2", "-b 0.0.0.0:8080", "-t 30"]
